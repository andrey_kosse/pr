<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

?>

<div>
    <h2>For generate some apples click here</h2>
    <div>
        <?= Html::beginForm(['/apple/generate'], 'post') ?>
        <input type="number" max="15" min="1" name="generate" value="1">
        <input type="submit" name="submit" class="btn btn-primary" value="Generate">
        <?= Html::endForm() ?>
    </div>

    <h2>Apples List</h2>
    <?php if (Yii::$app->session->hasFlash('eat-error')): ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
            <?php echo Yii::$app->session->getFlash('eat-error'); ?>
        </div>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'color',
            'status',
            'ate',
            [
                'label' => 'Size',
                'value' => function ($model) {
                    return ($model->sizeDecimal() . ' (' . $model->sizePercent() . ')');
                },
            ],
            [
                'label' => 'Drop',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->isHanging()) {
//                        return "<a href=\"/apple/drop/$model->id\">Упасть с дерева</a>";
                        return Html::a('Упасть с дерева', ['apple/drop', 'id' => $model->id]);
                    } else {
                        return "<p>Яблоко уже упало</p>";
                    }
                }
            ],
            [
                'label' => 'Eat percent',
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::beginForm(['/apple/eat/'. $model->id], 'post')
                        . "<div class=\"form-group\">
                        <input type=\"number\" max=\"100\" min=\"1\" name=\"percent\" value=\"1\">
                        <input type=\"submit\" name=\"submit\" class=\"btn btn-primary\" value=\"Eat\">
                        </div>" .
                        Html::endForm();
                }
            ],
        ],
    ]); ?>

</div>