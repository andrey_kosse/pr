<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property string|null $color
 * @property string $create_at
 * @property string|null $dropped_at
 * @property string|null $status
 * @property int|null $ate
 */
class AppleModel extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_at', 'dropped_at'], 'safe'],
            [['ate'], 'integer'],
            [['color', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Color',
            'create_at' => 'Create At',
            'dropped_at' => 'Dropped At',
            'status' => 'Status',
            'ate' => 'Ate',
        ];
    }

    const STATUS_HANGING = 'hanging';
    const STATUS_LIES = 'lies';
    const STATUS_ROTTEN = 'rotten';

    const APPLE_LIFE_CYCLE = '-5 hours';

    /**
     * @var array
     */
    private $availableColors = [
        'green', 'yellow', 'red'
    ];


    /**
     * AppleModel constructor.
     * @param string|null $color
     * @param array $config
     */
    public function __construct(string $color = null, $config = [])
    {
        parent::__construct($config);
        $this->color = $color ?: $this->randomColor();
    }


    /**
     * @return string
     */
    private function randomColor(): string
    {
        $color = $this->availableColors[array_rand($this->availableColors)];
        return $color;
    }


    /**
     *
     */
    public function fallToGround()
    {
        $this->status = 'lies';
        $this->dropped_at = date('Y-m-d H:i:s');
        $this->save();
    }


    /**
     * @return float
     */
    public function sizeDecimal(): float
    {
        return (100 - $this->ate) / 100;
    }

    /**
     * @return string
     */
    public function sizePercent(): string
    {
        return 100 - $this->ate . '%';
    }


    /**
     * @param int $percent
     * @return bool
     */
    public function tryToEat(int $percent = 100)
    {
        if ($this->canEat()) {
            $this->eat($percent);
            return true;
        } else {
            Yii::$app->session->setFlash('eat-error', "Can't eat. Apple is " . mb_strtoupper($this->status));
            return false;
        }
    }


    /**
     * @return bool
     */
    public function isHanging(): bool
    {
        return $this->status === self::STATUS_HANGING ? true : false;
    }


    /**
     * @param int $percent
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function eat(int $percent)
    {
        if ($percent >= 100 || ($this->ate + $percent) >= 100) {
            $this->delete();
        } else {
            $this->ate += $percent;
            $this->save();
        }
    }


    /**
     * @return bool
     */
    private function canEat(): bool
    {
        return $this->status === self::STATUS_LIES ? true : false;
    }


    /**
     * @param int $count
     * @return array
     */
    static public function generateApples(int $count = 10): array
    {
        $arrayOfApples = [];
        for ($i = 0; $i < $count; $i++) {
            $newApple = new AppleModel();
            $newApple->save();
            $arrayOfApples[] = $newApple;
        }
        return $arrayOfApples;
    }


    /**
     * @return array
     */
    static public function allApples(): array
    {
        self::checkStatusAll();
        $apples = AppleModel::find()->all();
        return $apples;
    }


    /**
     * @param $condition
     * @return ActiveRecord|null
     */
    static public function findOne($condition)
    {
        $apple = parent::findOne($condition);
        self::checkStatusOne($apple);
        return $apple;
    }


    /**
     *
     */
    static private function checkStatusAll()
    {
        $apples = AppleModel::find()
            ->where(['=', 'status', self::STATUS_LIES])
            ->andWhere(['<', 'dropped_at', self::getLifeCycleBorder()])
            ->all();

        foreach ($apples as $appleModel) {
            $appleModel->status = self::STATUS_ROTTEN;
            $appleModel->save();
        }
    }


    /**
     * @param AppleModel $apple
     */
    static private function checkStatusOne(AppleModel $apple)
    {
        if (($apple->status === self::STATUS_LIES) && (strtotime($apple->dropped_at) < self::getLifeCycleBorder())) {
            $apple->status = self::STATUS_ROTTEN;
        }
    }


    /**
     * @return false|string
     */
    static private function getLifeCycleBorder()
    {
        return date('Y-m-d H:i:s', strtotime(self::APPLE_LIFE_CYCLE));
    }


}