<?php

namespace backend\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\AppleModel;


class AppleController extends Controller{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionList(){
        $apples = AppleModel::allApples();
        $dataProvider = new ArrayDataProvider();
        $dataProvider->setModels($apples);
        return $this->render('list', ['apples' => $apples, 'dataProvider' => $dataProvider]);
    }

    public function actionGenerate(){
        $request = Yii::$app->request;
        if($request->isPost){
            $apples = AppleModel::generateApples($request->post('generate', 10));
        }
        return $this->redirect(['list']);
    }

    public function actionDrop($id){
        $apple = AppleModel::findOne($id);
        $apple->fallToGround();
        return $this->redirect(['list']);
    }

    public function actionEat($id){
        $request = Yii::$app->request;
        if($request->isPost){
            $percent = $request->post('percent', 0);
            $apple = AppleModel::findOne($id);
            $apple->tryToEat($percent);
        }
        return $this->redirect(['list']);
    }
}