<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property string|null $color
 * @property string $create_at
 * @property string|null $dropped_at
 * @property string|null $status
 * @property int|null $ate
 */
class Apple extends \yii\db\ActiveRecord
{

    public $color;
    public $create_at;
    public $dropped_at;
    public $status;
    public $ate;



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_at', 'dropped_at'], 'safe'],
            [['ate'], 'integer'],
            [['color', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Color',
            'create_at' => 'Create At',
            'dropped_at' => 'Dropped At',
            'status' => 'Status',
            'ate' => 'Ate',
        ];
    }
}
